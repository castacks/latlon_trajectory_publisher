#include "latlon_trajectory_creator/latlon_trajectory_creator.h"

int main(int argc, char *argv[]) {
    ros::init(argc, argv, "lat_lon");
    ros::NodeHandle n("~");
    ros::Rate loop_rate(10);
    LatLonTrajectoryCreator ltc;
    if(!ltc.Initialize(n)){
        ROS_ERROR_STREAM("Failed to Initialize!");
    }
    ros::spin();

    return 0;
}
