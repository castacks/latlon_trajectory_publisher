#include "latlon_trajectory_creator/latlon_trajectory_creator.h"

bool LatLonTrajectoryCreator::Initialize(ros::NodeHandle &n){
    bool server = false;
    if(!lfm_client_.Initialize(server,n)){
        ROS_ERROR_STREAM("Could not initialize server.");
        return false;
    }

    lookahead_mode_ = false;

    if (!n.getParam("pub_topic", pub_topic_)){
      ROS_ERROR_STREAM("Could not get pub_topic parameter.");
      return false;
    }
    trajectory_pub_ = n.advertise<ca_nav_msgs::PathXYZVViewPoint>(pub_topic_, 1);

    if (!n.getParam("sub_topic", sub_topic_)){
      ROS_ERROR_STREAM("Could not get sub_topic parameter.");
      return false;
    }
    activate_sub_ = n.subscribe(sub_topic_, 1,&LatLonTrajectoryCreator::StringCallback,this);


    if (!n.getParam("lookahead_topic", odo_sub_topic_)){
      ROS_ERROR_STREAM("Could not get lookahead_topic parameter.");
      return false;
    }
    odo_sub_ = n.subscribe(odo_sub_topic_, 1,&LatLonTrajectoryCreator::OdoCallback,this);


    if (!n.getParam("frame", frame_)){
      ROS_ERROR_STREAM("Could not get frame parameter.");
      return false;
    }

    if (!n.getParam("number_of_waypoints", num_wp_)){
      ROS_ERROR_STREAM("Could not get number_of_waypoints parameter.");
      return false;
    }
    if(num_wp_==0){
        ROS_ERROR_STREAM("Zero Waypoints, exiting");
        return false;
    }

    std::string lat_string = "lat_wp";
    std::string lon_string = "lon_wp";
    std::string height_string = "height_wp";
    std::string vel_string = "velocity_wp";

    std::string lat_viewpoint_string = "lat_viewpoint_wp";
    std::string lon_viewpoint_string = "lon_viewpoint_wp";
    std::string height_viewpoint_string = "height_viewpoint_wp";

    for(size_t i=0; i<num_wp_; i++){
        std::string temp_lat_string = lat_string;
        std::string temp_lon_string = lon_string;
        std::string temp_height_string = height_string;

        std::string temp_vel_string = vel_string;

        std::string temp_lat_viewpoint_string = lat_viewpoint_string;
        std::string temp_lon_viewpoint_string = lon_viewpoint_string;
        std::string temp_height_viewpoint_string = height_viewpoint_string;

        {
            std::stringstream ss; ss << i;
            temp_lat_string.append(ss.str());
            temp_lon_string.append(ss.str());
            temp_height_string.append(ss.str());

            temp_vel_string.append(ss.str());

            temp_lat_viewpoint_string.append(ss.str());
            temp_lon_viewpoint_string.append(ss.str());
            temp_height_viewpoint_string.append(ss.str());
        }

        geometry_msgs::Point p;
        if (!n.getParam(temp_lat_string, p.x)){
          ROS_ERROR_STREAM("Could not get "<< temp_lat_string<<".");
          return false;
        }
        if (!n.getParam(temp_lon_string, p.y)){
          ROS_ERROR_STREAM("Could not get "<< temp_lon_string<<".");
          return false;
        }

        if (!n.getParam(temp_height_string, p.z)){
          ROS_ERROR_STREAM("Could not get "<< temp_height_string<<".");
          return false;
        }

        pvector_.push_back(p);

        if (!n.getParam(temp_lat_viewpoint_string, p.x)){
          ROS_ERROR_STREAM("Could not get "<< temp_lat_viewpoint_string<<".");
          return false;
        }
        if (!n.getParam(temp_lon_viewpoint_string, p.y)){
          ROS_ERROR_STREAM("Could not get "<< temp_lon_viewpoint_string<<".");
          return false;
        }

        if (!n.getParam(temp_height_viewpoint_string, p.z)){
          ROS_ERROR_STREAM("Could not get "<< temp_height_viewpoint_string<<".");
          return false;
        }

        view_point_vector_.push_back(p);

        double vel;
        if (!n.getParam(temp_vel_string, vel)){
          ROS_ERROR_STREAM("Could not get "<< temp_vel_string<<".");
          return false;
        }
        velocity_.push_back(vel);
    }

    //set up dem file
    std::string dem_file;
    if (!n.getParam("dem_file", dem_file)){
      ROS_ERROR_STREAM("Could not get dem_file parameter.");
      return false;
    }

    if(!dem_reader_.loadFile(0,dem_file)){
        ROS_ERROR_STREAM("DEM file reading failed.");
        return false;
    }


    if (!n.getParam("viewpoint_based_heading", viewpoint_active_)){
      ROS_ERROR_STREAM("Could not get viewpoint_based_heading parameter.");
      return false;
    }


    ros::Rate r(10); // 10 hz
    while(!lfm_client_.IsInitialized() && ros::ok()){
        ros::spinOnce(); r.sleep();
    }

    PutLatLonPointsOnGround();
    initialized_ = true;
    return true;
}

bool LatLonTrajectoryCreator::PutLatLonPointsOnGround(){
    for(size_t i=0; i<pvector_.size(); i++){
        geometry_msgs::Point& p = pvector_[i];
        bool valid = false;
        p.z = dem_reader_.getHeight(p.x,p.y,&valid) - p.z;
        if(!valid){
            ROS_ERROR_STREAM("Cannot look up height!");
            return false;
        }

        lfm_client_.MSL2Altitude(p.z,p.z);
        Eigen::Vector3d v;
        ros::Rate loop_rate(10);
        while(!lfm_client_.TransformLatLon2LocalFrame(p.x*DEG2RAD_C,p.y*DEG2RAD_C,p.z,v,frame_)){
            ROS_ERROR_STREAM("Couldn't transform lat lon to "<<frame_);
            ros::spinOnce();
            loop_rate.sleep();
        }
        p.x = v.x(); p.y = v.y(); p.z = v.z();
    }

    for(size_t i=0; i<view_point_vector_.size(); i++){
        geometry_msgs::Point& p = view_point_vector_[i];
        bool valid = false;
        p.z = dem_reader_.getHeight(p.x,p.y,&valid) - p.z;
        if(!valid){
            ROS_ERROR_STREAM("Cannot look up height!");
            return false;
        }

        lfm_client_.MSL2Altitude(p.z,p.z);
        Eigen::Vector3d v;
        ros::Rate loop_rate(10);
        while(!lfm_client_.TransformLatLon2LocalFrame(p.x*DEG2RAD_C,p.y*DEG2RAD_C,p.z,v,frame_)){
            ROS_ERROR_STREAM("Couldn't transform lat lon to "<<frame_);
            ros::spinOnce();
            loop_rate.sleep();
        }
        p.x = v.x(); p.y = v.y(); p.z = v.z();
    }

    return true;

}

void LatLonTrajectoryCreator::OdoCallback(const nav_msgs::Odometry::ConstPtr& msg){
    got_lookahead_ = true;
    lookahead_p_ = msg->pose.pose.position;
}

void LatLonTrajectoryCreator::PublishTrajectory(){
    ca_nav_msgs::PathXYZVViewPoint path;
    path.header.stamp = ros::Time::now();
    path.header.frame_id = frame_;

    ca_nav_msgs::XYZVViewPoint wp;

    if(!got_lookahead_)
        ROS_WARN("Trjectory creator did not get the look ahead message, sending a trajectory anyways.");

    if(pvector_.size()>0){
        wp.position = lookahead_p_;
        wp.vel = velocity_[0];
        wp.viewpoint = view_point_vector_[0];
        wp.safety = !viewpoint_active_;
        path.waypoints.push_back(wp);
    }

    for(size_t i=0; i< pvector_.size(); i++){
        wp.position = pvector_[i];
        wp.vel = velocity_[i];
        wp.viewpoint = view_point_vector_[i];
        path.waypoints.push_back(wp);
    }

    trajectory_pub_.publish(path);
}


void LatLonTrajectoryCreator::StringCallback(const std_msgs::String::ConstPtr& msg){
    if(initialized_)
        PublishTrajectory();
}
