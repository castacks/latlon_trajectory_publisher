#ifndef LATLON_TRAJECTORY_CREATOR_H
#define LATLON_TRAJECTORY_CREATOR_H
#include "ros/ros.h"
#include "ca_nav_msgs/PathXYZVViewPoint.h"
#include "tf_utils/tf_utils.h"
#include "local_frame_manager/local_frame_manager.h"
#include "vector"
#include "dem_reader/dem.h"
#include "std_msgs/String.h"
#include "nav_msgs/Odometry.h"

class LatLonTrajectoryCreator
{    
    std::vector<geometry_msgs::Point> pvector_;
    std::vector<geometry_msgs::Point> view_point_vector_;
    std::vector<double> velocity_;
    bool viewpoint_active_;

    std::string frame_;
    std::string pub_topic_;
    std::string odo_sub_topic_;
    std::string sub_topic_;
    int num_wp_;
    ros::Publisher trajectory_pub_;
    ros::Subscriber activate_sub_;
    ros::Subscriber odo_sub_;
    DEMData dem_reader_;

    bool lookahead_mode_; //Fixed or lookahead-- For now just supporting fixed
    bool initialized_;
    bool got_lookahead_;
    geometry_msgs::Point lookahead_p_;
public:
    void StringCallback(const std_msgs::String::ConstPtr& msg);
    void OdoCallback(const nav_msgs::Odometry::ConstPtr& msg);
    LocalFrameManager lfm_client_;
    LatLonTrajectoryCreator()
    {   initialized_ = false;
        lookahead_p_.x = 0;
        lookahead_p_.y = 0;
        lookahead_p_.z = 0;
        got_lookahead_ = false;
    }
    bool Initialize(ros::NodeHandle& n);
    bool PutLatLonPointsOnGround();
    void PublishTrajectory();
};

#endif // LATLONVISUALIZER_H
